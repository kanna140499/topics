<!DOCTYPE html>
    <head>
        <title>Laravel</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.1/css/bootstrap.min.css">
        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.1/js/bootstrap.min.js"></script>
        <meta name="csrf-token" content="{{ csrf_token() }}">
    </head>
    <body>
        <div class="container">
        <div class="card mt-3">
            <div class="card-header"><h2><a href="{{ url('/topics') }}">{{ $topic->name }}</a></h2></div>
                <div class="card-body">
                    <h3>Sub Topics</h3>
                    <form action="{{ url('add-remove-multiple-input-fields', ['topic' => $topic->id]) }}" method="POST">
                        {{ csrf_field() }}
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        @if (Session::has('success'))
                            <div class="alert alert-success text-center">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                                <p>{{ Session::get('success') }}</p>
                            </div>
                        @endif
                        <table class="table table-bordered" id="dynamicAddRemove">
                            <tr>
                                <th>Title</th>
                                <th>Description</th>
                                <th>Action</th>
                            </tr>
                            <?php
                            if (isset($topic->subtopic)) {
                                foreach ($topic->subtopic as $subtopic) { ?>
                                    <tr>
                                        <td><input type="text" name="moreFields[0][title]" class="form-control"  value="{{ $subtopic->name }}" /></td>
                                        <td><input type="text" name="moreFields[0][description]" class="form-control"  value="{{ $subtopic->description }}" /></td>
                                        <td>
                                            <form action="{{ route('subtopics.destroy',$subtopic->id) }}" method="POST">
                                                {{ method_field('DELETE') }}
                                                {{ csrf_field() }}
                                                <input type="hidden" name="id" value="{{ $subtopic->id }}">
                                                <button type="submit" class="btn btn-danger">Delete</button>
                                            </form>
                                        </td>
                                    </tr>
                                <?php
                                }
                            } ?>
                            <tr>
                                <td><input type="text" name="moreFields[0][title]" placeholder="Enter title" class="form-control" required/></td>
                                <td><input type="text" name="moreFields[0][description]" placeholder="Enter description" class="form-control"  required/></td>
                                <td><button type="button" name="add" id="add-btn" class="btn btn-primary" >Add More</button></td>
                            </tr>
                        </table>
                        <button type="submit" class="btn btn-success">Save</button>
                    </form>
                </div>
            </div>
        </div>
        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}"></script>
        <script type="text/javascript">
            var i = 0;
            $("#add-btn").click(function(){
            ++i;
            $("#dynamicAddRemove").append('<tr><td><input type="text" name="moreFields['+i+'][title]" placeholder="Enter title" class="form-control" /></td><td><input type="text" name="moreFields['+i+'][description]" placeholder="Enter description" class="form-control" /></td><td><button type="button" class="btn btn-danger remove-tr">Remove</button></td></tr>');
            });
            $(document).on('click', '.remove-tr', function(){
            $(this).parents('tr').remove();
            });
        </script>
    </body>
</html>