@extends('layouts.app')

@if($errors->any())
    <div class="alert alert-danger">
        <ul>
            {{ implode('', $errors->all(':message')) }}
        </ul>
    </div>
@endif

@section('content')
    <h1>Add New Topic</h1>
    <form action="{{ route('topics.store') }}" method="POST">
        {{ csrf_field() }}
        <!-- @csrf -->
        <div class="form-group">
            <label for="name">Name:</label>
            <input type="text" class="form-control" id="name" name="name" required>
        </div>
        <button type="submit" class="btn btn-primary">Add Topic</button>
    </form>
@endsection
