@extends('layouts.app')

@if($errors->any())
    <div class="alert alert-danger">
        <ul>
            {{ implode('', $errors->all(':message')) }}
        </ul>
    </div>
@endif

@section('content')
    <h1>Edit Topic</h1>

    <form action="/topics/{{ $topic->id }}" method="POST">
        {{ method_field('PUT') }}
        {{ csrf_field() }}
        <!-- @csrf
        @method('PUT') -->
        <div class="form-group">
            <label for="name">Name:</label>
            <input type="text" class="form-control" id="name" name="name" value="{{ $topic->name }}" required>
        </div>
        <button type="submit" class="btn btn-primary">Update</button>
    </form>
@endsection
