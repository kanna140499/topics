<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/topics', 'TopicController@index');
Route::get('/topics/create', 'TopicController@create')->name('topics.create');
Route::post('/topics', 'TopicController@store')->name('topics.store');
Route::get('/topics/{topic}', 'TopicController@show')->name('topics.show');
Route::get('/topics/{topic}/edit', 'TopicController@edit')->name('topics.edit');
Route::put('/topics/{topic}', 'TopicController@update')->name('topics.update');
Route::delete('/topics/{topic}', 'TopicController@destroy')->name('topics.destroy');
Route::post('/add-remove-multiple-input-fields/{topic}', 'TopicController@store_subtopic');
Route::delete('/subtopics/{topic}', 'TopicController@destroy_subtopic')->name('subtopics.destroy');
