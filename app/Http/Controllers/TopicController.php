<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Topic;
use App\Subtopic;
use Illuminate\Support\Facades\Validator;
use Redirect;

class TopicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $topics = Topic::all();
        return view('topics.index', compact('topics'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('topics.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' =>'required|string|min:1|max:70'
        ]
        );
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator);
        }
        $topic = new Topic();
        $topic->name = $request->input('name');
        $topic->save();

        return redirect('/topics');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Topic $topic)
    {
        $topic = Topic::with('subtopic')->find($topic->id);
        return view('topics.show', compact('topic'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Topic $topic)
    {
        return view('topics.edit', compact('topic'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Topic $topic)
    {
        $validator = Validator::make($request->all(), [
            'name' =>'string|min:1|max:70'
        ]
        );
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator);
        }
        $topic->name = $request->input('name');
        $topic->save();

        return redirect('/topics');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Topic $topic)
    {
        $topic->delete();

        return redirect('/topics');
    }

    public function store_subtopic(Request $request, Topic $topic)
    {
        $validator = Validator::make($request->all(), [
            'moreFields.*.title' => 'required'
        ]
        );
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator);
        }
        foreach ($request->moreFields as $key => $value) {
            $subtopic = new Subtopic();
            $subtopic->topic_id =$topic->id;
            $subtopic->name = $value['title'];
            $subtopic->description = $value['description'];
            $subtopic->save();
        }
        return back()->with('success', 'New subtopic has been added.');
    }
    public function destroy_subtopic(Request $request, $subtopic)
    {
        Subtopic::where('id', $subtopic)->delete();

        return back()->with('success', 'Subtopic has been removed.');
    }
}
