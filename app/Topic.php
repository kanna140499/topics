<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Topic extends Model
{
    protected $fillable = [
        'name',
    ];

    public function subtopic()
    {
        return $this->hasMany(Subtopic::class, 'topic_id', 'id');
    }
}
